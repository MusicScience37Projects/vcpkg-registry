#!/usr/bin/env python3
"""Test installation of a package."""

import pathlib
import subprocess

import click

THIS_DIR = pathlib.Path(__file__).absolute().parent
ROOT_DIR = THIS_DIR.parent
VCPKG_PATH = ROOT_DIR / "vcpkg" / "vcpkg"


@click.command()
@click.argument("port_name", nargs=1)
def test_install(port_name: str) -> None:
    """Test installation of a package."""
    subprocess.run(
        [
            str(VCPKG_PATH),
            "remove",
            port_name,
        ],
        check=True,
        cwd=ROOT_DIR,
    )
    subprocess.run(
        [
            str(VCPKG_PATH),
            "install",
            port_name,
            "--overlay-ports=ports/cpp-hash-tables",
            "--overlay-ports=ports/cpp-stat-bench",
            "--overlay-ports=ports/cpp-msgpack-rpc",
            "--overlay-ports=ports/numerical-collection-cpp",
            "--overlay-ports=ports/cpp-msgpack-light",
        ],
        check=True,
        cwd=ROOT_DIR,
    )


if __name__ == "__main__":
    test_install()
