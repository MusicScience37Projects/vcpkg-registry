vcpkg_from_gitlab(
    GITLAB_URL
    https://gitlab.com
    OUT_SOURCE_PATH
    SOURCE_PATH
    REPO
    MusicScience37Projects/utility-libraries/cpp-stat-bench
    REF
    v0.22.0
    SHA512
    ca8cdf2398ff2affa20d4e9adf61d6b0dd100fbd722f55c1e57cab13740936deda5bb15163b084f3e22a6489e476694a0ab14fe64a2534a88271611e7a2cd657
    HEAD_REF
    main)

vcpkg_cmake_configure(SOURCE_PATH "${SOURCE_PATH}")
vcpkg_cmake_build()
vcpkg_cmake_install()
vcpkg_cmake_config_fixup(CONFIG_PATH lib/cmake/cpp-stat-bench PACKAGE_NAME
                         cpp-stat-bench)
file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/debug/include")
vcpkg_copy_pdbs()
vcpkg_install_copyright(FILE_LIST "${SOURCE_PATH}/LICENSE.txt")
