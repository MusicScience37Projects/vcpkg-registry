vcpkg_from_gitlab(
    GITLAB_URL
    https://gitlab.com
    OUT_SOURCE_PATH
    SOURCE_PATH
    REPO
    MusicScience37Projects/utility-libraries/cpp-hash-tables
    REF
    v0.10.0
    SHA512
    a14122ba67a9ff68f1337ac2c4750a370ca87f024b4ce83e548d95a4c8738ce07ee222d42c07530810d7c46b58dd037ee6e6ed1192e66bc9052557011486d39d
    HEAD_REF
    develop)

vcpkg_cmake_configure(SOURCE_PATH "${SOURCE_PATH}")
vcpkg_cmake_build()
vcpkg_cmake_install()
vcpkg_cmake_config_fixup(CONFIG_PATH lib/cmake/cpp-hash-tables PACKAGE_NAME
                         cpp-hash-tables)
file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/debug"
     "${CURRENT_PACKAGES_DIR}/lib")
vcpkg_install_copyright(FILE_LIST "${SOURCE_PATH}/LICENSE.txt")
