vcpkg_from_gitlab(
    GITLAB_URL
    https://gitlab.com
    OUT_SOURCE_PATH
    SOURCE_PATH
    REPO
    MusicScience37Projects/utility-libraries/cpp-msgpack-rpc
    REF
    v0.2.0
    SHA512
    898a850f0af264b7a355d64614d82ce29505c8a250d5e95eee5e42d9b6bf79135d7ae32c3c4c7f383f1fa7eb579b4810f3aaa27deceb85a5929dbc1ff5d6fdd2
    HEAD_REF
    develop)

vcpkg_cmake_configure(SOURCE_PATH "${SOURCE_PATH}")
vcpkg_cmake_build()
vcpkg_cmake_install()
vcpkg_cmake_config_fixup(CONFIG_PATH lib/cmake/cpp-msgpack-rpc PACKAGE_NAME
                         cpp-msgpack-rpc)
file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/debug/include")
vcpkg_copy_pdbs()
vcpkg_install_copyright(FILE_LIST "${SOURCE_PATH}/LICENSE.txt")
configure_file("${CMAKE_CURRENT_LIST_DIR}/usage"
               "${CURRENT_PACKAGES_DIR}/share/${PORT}/usage" COPYONLY)
