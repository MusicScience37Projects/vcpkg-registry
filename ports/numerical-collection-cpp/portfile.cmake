vcpkg_from_gitlab(
    GITLAB_URL
    https://gitlab.com
    OUT_SOURCE_PATH
    SOURCE_PATH
    REPO
    MusicScience37Projects/numerical-analysis/numerical-collection-cpp
    REF
    v0.10.0
    SHA512
    c650793c304783863da46231b98d34b66139f75e37f4d01ecb412381d0ed6c7e520bd4bc88b130c13ec8750685b3a53f99a620ed036eac0ae3556644a765916a
    HEAD_REF
    main)

vcpkg_cmake_configure(SOURCE_PATH "${SOURCE_PATH}")
vcpkg_cmake_build()
vcpkg_cmake_install()
vcpkg_cmake_config_fixup(CONFIG_PATH lib/cmake/num-collect PACKAGE_NAME
                         num-collect)
file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/debug/include")
vcpkg_copy_pdbs()
vcpkg_install_copyright(FILE_LIST "${SOURCE_PATH}/LICENSE.txt")
