vcpkg_from_gitlab(
    GITLAB_URL
    https://gitlab.com
    OUT_SOURCE_PATH
    SOURCE_PATH
    REPO
    MusicScience37Projects/utility-libraries/cpp-msgpack-light
    REF
    v0.3.0
    SHA512
    192a1c3004b9aea27ff005c8bdcdc49a68ce258049d45e29f4e6fc600b91728e054b06c753ec5508ae9e57b54e98e55cebdb16d1ab5424174de688f9436318ea
    HEAD_REF
    main)

vcpkg_cmake_configure(SOURCE_PATH "${SOURCE_PATH}")
vcpkg_cmake_build()
vcpkg_cmake_install()
vcpkg_cmake_config_fixup(CONFIG_PATH lib/cmake/cpp-msgpack-light PACKAGE_NAME
                         cpp-msgpack-light)
file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/debug"
     "${CURRENT_PACKAGES_DIR}/lib")
vcpkg_install_copyright(FILE_LIST "${SOURCE_PATH}/LICENSE.txt")
