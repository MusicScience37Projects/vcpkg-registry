# vcpkg registry

vcpkg registry of MusicScience37's projects.

## Usage

One can use packages in this registry by configuring in `vcpkg-configuration.json` file.

Example:

```json
{
  "$schema": "https://raw.githubusercontent.com/microsoft/vcpkg-tool/main/docs/vcpkg-configuration.schema.json",
  "registries": [
    {
      "kind": "git",
      "repository": "https://gitlab.com/MusicScience37Projects/vcpkg-registry",
      "baseline": "6fa7c134e139b667cd8260d533981b23c1ed5ff0",
      "packages": [
        "cpp-hash-tables",
        "cpp-msgpack-light",
        "cpp-msgpack-rpc",
        "cpp-stat-bench",
        "numerical-collection-cpp"
      ]
    }
  ]
}
```

`"baseline"` should be changed to the latest commit in this repository.

## For developer of this repository

### Install vcpkg

1. Download vcpkg repository.

   ```bash
   git submodule update --init
   ```

2. Build vcpkg.

   - On Windows:

     ```bat
     .\vcpkg\bootstrap-vcpkg.bat
     ```

   - On Linux:

     ```bash
     ./vcpkg/bootstrap-vcpkg.sh
     ```

### Test a package

```bash
./scripts/test_install.py <port-name>
```

### Get git tree

```bash
git rev-parse <commit>:ports/<port-name>
```
